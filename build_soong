diff --git a/android/arch.go b/android/arch.go
index ad812a4..659b41b 100644
--- a/android/arch.go
+++ b/android/arch.go
@@ -115,6 +115,7 @@ var archVariants = map[ArchType][]string{
 		"cortex-a53",
 		"cortex-a53-a57",
 		"cortex-a55",
+		"cortex-a57",
 		"cortex-a72",
 		"cortex-a73",
 		"cortex-a75",
@@ -130,6 +131,7 @@ var archVariants = map[ArchType][]string{
 		"armv8_2a",
 		"cortex-a53",
 		"cortex-a55",
+		"cortex-a57",
 		"cortex-a72",
 		"cortex-a73",
 		"cortex-a75",
@@ -1302,6 +1304,7 @@ func getMegaDeviceConfig() []archConfig {
 		{"arm", "armv7-a-neon", "cortex-a15", []string{"armeabi-v7a"}},
 		{"arm", "armv7-a-neon", "cortex-a53", []string{"armeabi-v7a"}},
 		{"arm", "armv7-a-neon", "cortex-a53.a57", []string{"armeabi-v7a"}},
+		{"arm", "armv7-a-neon", "cortex-a57", []string{"armeabi-v7a"}},
 		{"arm", "armv7-a-neon", "cortex-a72", []string{"armeabi-v7a"}},
 		{"arm", "armv7-a-neon", "cortex-a73", []string{"armeabi-v7a"}},
 		{"arm", "armv7-a-neon", "cortex-a75", []string{"armeabi-v7a"}},
@@ -1312,6 +1315,7 @@ func getMegaDeviceConfig() []archConfig {
 		{"arm", "armv7-a-neon", "exynos-m1", []string{"armeabi-v7a"}},
 		{"arm", "armv7-a-neon", "exynos-m2", []string{"armeabi-v7a"}},
 		{"arm64", "armv8-a", "cortex-a53", []string{"arm64-v8a"}},
+		{"arm64", "armv8-a", "cortex-a57", []string{"arm64-v8a"}},
 		{"arm64", "armv8-a", "cortex-a72", []string{"arm64-v8a"}},
 		{"arm64", "armv8-a", "cortex-a73", []string{"arm64-v8a"}},
 		{"arm64", "armv8-a", "kryo", []string{"arm64-v8a"}},
diff --git a/cc/config/arm64_device.go b/cc/config/arm64_device.go
index 1ca1656..59c4ff5 100644
--- a/cc/config/arm64_device.go
+++ b/cc/config/arm64_device.go
@@ -55,6 +55,9 @@ var (
 		"cortex-a55": []string{
 			"-mcpu=cortex-a55",
 		},
+		"cortex-a57": []string{
+			"-mcpu=cortex-a57",
+		},
 		"cortex-a75": []string{
 			// Use the cortex-a55 since it is similar to the little
 			// core (cortex-a55) and is sensitive to ordering.
@@ -109,6 +112,9 @@ func init() {
 	pctx.StaticVariable("Arm64ClangCortexA55Cflags",
 		strings.Join(arm64ClangCpuVariantCflags["cortex-a55"], " "))
 
+	pctx.StaticVariable("Arm64ClangCortexA57Cflags",
+		strings.Join(arm64ClangCpuVariantCflags["cortex-a57"], " "))
+
 	pctx.StaticVariable("Arm64ClangKryoCflags",
 		strings.Join(arm64ClangCpuVariantCflags["kryo"], " "))
 
@@ -129,6 +135,7 @@ var (
 		"":           "",
 		"cortex-a53": "${config.Arm64ClangCortexA53Cflags}",
 		"cortex-a55": "${config.Arm64ClangCortexA55Cflags}",
+		"cortex-a57": "${config.Arm64ClangCortexA57Cflags}",
 		"cortex-a72": "${config.Arm64ClangCortexA53Cflags}",
 		"cortex-a73": "${config.Arm64ClangCortexA53Cflags}",
 		"cortex-a75": "${config.Arm64ClangCortexA55Cflags}",
diff --git a/cc/config/arm_device.go b/cc/config/arm_device.go
index aee16eb..dfb14ec 100644
--- a/cc/config/arm_device.go
+++ b/cc/config/arm_device.go
@@ -108,6 +108,15 @@ var (
 			// better solution comes around. See Bug 27340895
 			"-D__ARM_FEATURE_LPAE=1",
 		},
+		"cortex-a57": []string{
+			"-mcpu=cortex-a57",
+			"-mfpu=neon-fp-armv8",
+			// Fake an ARM compiler flag as these processors support LPAE which GCC/clang
+			// don't advertise.
+			// TODO This is a hack and we need to add it for each processor that supports LPAE until some
+			// better solution comes around. See Bug 27340895
+			"-D__ARM_FEATURE_LPAE=1",
+		},
 		"cortex-a75": []string{
 			"-mcpu=cortex-a55",
 			"-mfpu=neon-fp-armv8",
@@ -204,6 +213,8 @@ func init() {
 		strings.Join(armClangCpuVariantCflags["cortex-a53"], " "))
 	pctx.StaticVariable("ArmClangCortexA55Cflags",
 		strings.Join(armClangCpuVariantCflags["cortex-a55"], " "))
+	pctx.StaticVariable("ArmClangCortexA57Cflags",
+		strings.Join(armClangCpuVariantCflags["cortex-a57"], " "))
 	pctx.StaticVariable("ArmClangKraitCflags",
 		strings.Join(armClangCpuVariantCflags["krait"], " "))
 	pctx.StaticVariable("ArmClangKryoCflags",
@@ -225,6 +236,7 @@ var (
 		"cortex-a53":     "${config.ArmClangCortexA53Cflags}",
 		"cortex-a53.a57": "${config.ArmClangCortexA53Cflags}",
 		"cortex-a55":     "${config.ArmClangCortexA55Cflags}",
+		"cortex-a57":     "${config.ArmClangCortexA57Cflags}",
 		"cortex-a72":     "${config.ArmClangCortexA53Cflags}",
 		"cortex-a73":     "${config.ArmClangCortexA53Cflags}",
 		"cortex-a75":     "${config.ArmClangCortexA55Cflags}",
